package repository;

import domain.Category;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static utilities.DataBaseUtil.*;

public class CategoryDAO {

    private Connection connection;

    public CategoryDAO() {
        try {
            connection = DriverManager.getConnection(URL, USERNAME, "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected CategoryDAO(Connection connection) {
        this.connection = connection;
    }

    /**
     * Opzoeking categorie op id
     * @param id id van gezochte categorie
     * @return object categorie
     */
    public Category findCategoryById(int id) {
        Category category = new Category();
        try {
            PreparedStatement s = connection.prepareStatement("SELECT * FROM category WHERE id = ?");
            s.setInt(1, id);
            s.execute();
            ResultSet rs = s.getResultSet();
            if (rs.next()) {
                category = new Category();
                category.setId(rs.getInt("id"));
                category.setCategoryName(rs.getString("category_name"));
            }

            s.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return category;
    }
    /**
     * Opzoeking alle categorien
     * @param match start categorienaam
     * @return Arraylist van categorie object
     */
    public List<Category> findCategoriesWhereNameStartsWith(String match) {
        List<Category> matches = new ArrayList<>();
        try {
            PreparedStatement s = connection.prepareStatement("SELECT * FROM category WHERE category_name LIKE ?");
            s.setString(1, match + "%");
            s.execute();
            ResultSet rs = s.getResultSet();
            while (rs.next()) {

                Category c = new Category();
                c.setId(rs.getInt("id"));
                c.setCategoryName(rs.getString("category_name"));
                matches.add(c);
            }

            s.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return matches;
    }

}