package repository;

import domain.Popularity;
import utilities.DataBaseUtil;

import java.sql.*;

public class PopularityDAO {
    private Connection connection;

    public PopularityDAO(){
        try {
            connection = DriverManager.getConnection(DataBaseUtil.URL, DataBaseUtil.USERNAME, "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public PopularityDAO(Connection connection){
        this.connection = connection;
    }

    /**
     * Opzoeking popularity met id
     * @param id popularity id
     * @return popularity object
     */
    public Popularity findPopularityById(int id){
        Popularity popularity = new Popularity();
        try {
            PreparedStatement s = connection.prepareStatement("SELECT * FROM popularity WHERE id = ?");
            s.setInt(1, id);
            s.execute();
            ResultSet rs = s.getResultSet();
            if(rs.next()){
                popularity = new Popularity();
                popularity.setId(rs.getInt("id"));
                popularity.setPopularityName(rs.getString("popularity_name"));
            }
            s.close();
            return popularity;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return popularity;
    }
}


