package repository;

import domain.Category;
import domain.Popularity;
import domain.Series;
import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;
import services.CategoryService;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static utilities.DataBaseUtil.URL;
import static utilities.DataBaseUtil.USERNAME;

public class SeriesDAO {


    private Connection connection;

    public SeriesDAO() {
        try {
            this.connection = DriverManager.getConnection(URL, USERNAME, "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected SeriesDAO(Connection connection) {
        this.connection = connection;
    }


    /**
     * Opzoeking serie met bijhorend id
     * @param id id van gezochte serie
     * @return Serie object
     */
    public Series findSeriesByID(int id) {
        Series series = null;
        try {
            String query = "SELECT * FROM series g " +
                    "LEFT JOIN category c ON g.category_id = c.id " +
                    "LEFT JOIN popularity d ON g.popularity_id = d.id " +
                    "WHERE g.id = ? ";
            PreparedStatement s = connection.prepareStatement(query);
            s.setInt(1, id);
            s.execute();
            ResultSet rs = s.getResultSet();

            if (rs.next()) {
                series = this.createObjectFromSQL(series, rs);
            }
            s.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return series;
    }

    public List<Series> findAllSeries(){
        List<Series> series = new ArrayList<>();
        try{
            String query = "SELECT * FROM series g " +
                    "LEFT JOIN category c ON g.category_id = c.id " +
                    "LEFT JOIN popularity d ON g.popularity_id = d.id " +
                    "ORDER BY series_name ASC ";
            PreparedStatement s = connection.prepareStatement(query);
            s.execute();

            ResultSet rs = s.getResultSet();
            while (rs.next()) {
                Series serie = createObjectFromSQL(new Series(), rs);
                series.add(serie);
            }
            s.close();
            return series;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return series;
    }
    /**
     * Opzoeking alle series volgens jaar
     * @param year jaar
     * @return arraylist
     */
    public List<Series> showSeriesFromYear(int year){
        List<Series> serie = new ArrayList<>();
        int offset = year * 5-5;

        try {

            String query = "SELECT * FROM series g LEFT JOIN popularity d ON g.popularity_id = d.id LEFT JOIN category c ON g.category_id = c.id" +
                    "       ORDER BY series_name " +
                    "       LIMIT 5 OFFSET " + offset;
            PreparedStatement s = connection.prepareStatement(query);
            s.execute();
            ResultSet rs = s.getResultSet();

            while(rs.next()){
                Series series = createObjectFromSQL(new Series(), rs);
                serie.add(series);
            }
            s.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return serie;
    }

    /**
     * Opzoeken eertse serie met opgegeven String
     * @param match string begin naam serie
     * @return object serie door rji 1 DB
     */
    public Series findFirstSeriesStartingWith(String match){
        Series series = null;
        try{
            String query = "SELECT * FROM series g " +
                    "LEFT JOIN category c ON g.category_id = c.id " +
                    "LEFT JOIN popularity d ON g.popularity_id = d.id " +
                    "WHERE series_name LIKE ? " +
                    "ORDER BY series_name ASC LIMIT 1";
            PreparedStatement s = connection.prepareStatement(query);
            s.setString(1, match + "%");
            s.execute();

            ResultSet rs = s.getResultSet();
            if(rs.next()){
                series = createObjectFromSQL(new Series(), rs);
            }
            s.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return series;
    }
    /**
     * Aanmaken Series object
     * @param series
     * @param rs locatie aanmaken series
     * @return
     */
    private Series createObjectFromSQL(Series series, ResultSet rs){
        try{
            series = new domain.Series();
            series.setId(rs.getInt("id"));
            series.setSeriesName(rs.getString("series_name"));
            series.setDirector(rs.getString("director"));
            series.setYear(rs.getInt("year_edition"));
            series.setCategory(rs.getString("category_id"));
            series.setEpisodes(rs.getInt("episodes"));
            series.setNetwork(rs.getString("network"));
            series.setPopularity(rs.getInt("popularity_id"));


            if(rs.getInt("category_id") == 0){
                series.setCategory((Category) null);
            } else {
                Category c = new Category();
                c.setId(rs.getInt("c.id"));
                c.setCategoryName(rs.getString("c.category_name"));
                series.setCategory(c);
            }

            if(rs.getInt("popularity_id") == 0){
                series.setPopularity(null);
            } else {
                Popularity d = new Popularity();
                d.setId(rs.getInt("d.id"));
                d.setPopularity(rs.getString("d.popularity_name"));
                series.setPopularity(d);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return series;
    }
}
