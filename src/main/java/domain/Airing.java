package domain;

import java.util.Date;

public class Airing {
    private int id;
    private Series series;
    private Date startDate;
    private Date endDate;

    public Airing() {
    }

    public Airing(int id, Series series, Date startDate, Date endDate) {
        this.id = id;
        this.series = series;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Series getSeries() {
        return series;
    }

    public void setSeries(Series series) {
        this.series = series;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "Airing{" +
                "id=" + id +
                ", series=" + series +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
