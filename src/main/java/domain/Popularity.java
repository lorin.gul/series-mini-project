package domain;

public class Popularity {
    private int id;
    private String popularityName;

    public Popularity(int id, String popularityName) {
        this.id = id;
        this.popularityName = popularityName;
    }

    public Popularity() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPopularityName() {
        return popularityName;
    }

    public void setPopularityName(String popularityName) {
        this.popularityName = popularityName;
    }

    @Override
    public String toString() {
        return "Popularity{" +
                "id=" + id +
                ", popularityName='" + popularityName + '\'' +
                '}';
    }

    public void setPopularity(String string) {
    }
}
