package domain;

public class Series {
   private int id;
   private String seriesName;
   private String director;
   private int year;
   private Category category;
   private int episodes;
   private String network;
   private Popularity popularity;

    public Series() {
    }

    public Series(int id, String seriesName, String director, int year, Category category, int episodes, String network, Popularity popularity) {
        this.id = id;
        this.seriesName = seriesName;
        this.director = director;
        this.year = year;
        this.category = category;
        this.episodes = episodes;
        this.network = network;
        this.popularity = popularity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSeriesName() {
        return seriesName;
    }

    public void setSeriesName(String seriesName) {
        this.seriesName = seriesName;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public int getEpisodes() {
        return episodes;
    }

    public void setEpisodes(int episodes) {
        this.episodes = episodes;
    }

    public String getNetwork() {
        return network;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    public Popularity getPopularity() {
        return popularity;
    }

    public void setPopularity(Popularity popularity) {
        this.popularity = popularity;
    }

    @Override
    public String toString() {
        return "Series{" +
                "id=" + id +
                ", seriesName='" + seriesName + '\'' +
                ", director='" + director + '\'' +
                ", year=" + year +
                ", category=" + category +
                ", episodes=" + episodes +
                ", network='" + network + '\'' +
                ", popularity=" + popularity +
                '}';
    }

    public void setCategory(String category_id) {
    }

    public void setPopularity(int popularity_id) {
    }
}
