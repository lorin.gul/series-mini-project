package services;

import domain.Series;
import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;
import repository.SeriesDAO;

import java.util.List;

public class SeriesService {

    private static SeriesDAO seriesDAO;

    public SeriesService(SeriesDAO seriesDAO) {
        this.seriesDAO = seriesDAO;
    }

    public static Series findSeriesByID(int id) throws IDOutOfBoundException, NoRecordFoundException {
        if (id <= 0) {
            throw new IDOutOfBoundException();
        } else {
            Series game = seriesDAO.findSeriesByID(id);
            if (game == null) {
                throw new NoRecordFoundException();
            } else {
                return game;
            }
        }
    }
    /**
     * Zoekt alle series
     * @return arraylist series object
     * @throws NoRecordFoundException indien geen resultaat
     */
    public List<Series> findAllSeries() throws NoRecordFoundException {
        List<Series> series = seriesDAO.findAllSeries();

        if(series.isEmpty()){
            throw new NoRecordFoundException();
        } else {
            return series;
        }
    }

    /**
     * Weergave series op gekozen jaar
     * arraylist: 5 af te trekken van het product van het pagina nummer maal 5.
     * @param year jaar
     * @return arraylist
     * @throws NoRecordFoundException indien geen resultaat
     */
    public List<Series> showSeriesFromYear(int year) throws NoRecordFoundException {
        if(year <= 0){
            throw new IllegalArgumentException();
        } else {
            List<Series> series = seriesDAO.showSeriesFromYear(year);
            if(series.isEmpty()){
                throw new NoRecordFoundException();
            } else {
                return series;
            }
        }
    }
    /**
     * Opzoeking serie met opgegeven String
     * @param match Begin String serie
     * @return Series object
     * @throws NoRecordFoundException indien geen resultaat
     */
    public Series findSeriesStartingWith(String match) throws NoRecordFoundException {
        if(match.isEmpty()){
            throw new IllegalArgumentException();
        } else {
            Series series = seriesDAO.findFirstSeriesStartingWith(match);
            if(series == null){
                throw new NoRecordFoundException();
            } else {
                return series;
            }
        }
    }
}
