package services;

import domain.Popularity;
import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;
import repository.PopularityDAO;

public class PopularityService {
    private PopularityDAO popularityDAO;

    /**
     * Aanmaken PopularityService object
     * @param popularityDAO verbinding DB
     */
    public PopularityService(PopularityDAO popularityDAO){
        this.popularityDAO = popularityDAO;
    }

    /**
     * Opzoeking
     * @param id gezochte popularity
     * @return domain.Popularity object
     * @throws IDOutOfBoundException id kleiner of gelijk aan 0
     * @throws NoRecordFoundException geen resultaat
     */
    public Popularity findPopularityById(int id) throws IDOutOfBoundException, NoRecordFoundException {
        if(id <= 0){
            throw new IDOutOfBoundException();
        } else {
            Popularity popularity = popularityDAO.findPopularityById(id);
            if(popularity == null){
                throw new NoRecordFoundException();
            } else {
                return popularity;
            }
        }
    }
}


