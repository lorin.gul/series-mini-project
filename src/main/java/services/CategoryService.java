package services;

import domain.Category;
import exceptions.NoRecordFoundException;
import repository.CategoryDAO;

import java.util.List;

public class CategoryService {
    private CategoryDAO categoryDAO;

    /**
     * Aanmaken nieuw object
     * @param categoryDAO verbinding met DB
     */
    public CategoryService(CategoryDAO categoryDAO) {
        this.categoryDAO = categoryDAO;
    }

    /**
     * opzoeking categorie op id
     * @param id id gezochte categorie
     * @return abject categorie
     * @throws NoRecordFoundException indien geen resultaat
     */
    public Category findCategoryById(int id) throws NoRecordFoundException {
        Category category = categoryDAO.findCategoryById(id);
        if (category == null) {
            throw new NoRecordFoundException();
        } else {
            return category;
        }
    }
    public List<Category> findCategoryStartingWith(String match) throws NoRecordFoundException {
        List<Category> matches =  categoryDAO.findCategoriesWhereNameStartsWith(match);
        if(matches.isEmpty() || matches == null){
            throw new NoRecordFoundException();
        } else {
            return matches;
        }
    }

}