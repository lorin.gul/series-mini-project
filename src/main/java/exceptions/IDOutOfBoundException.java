package exceptions;

public class IDOutOfBoundException extends Exception {

    public IDOutOfBoundException() {
        super("ID is too small/large");
    }
    public String getMessage() {
        String message = "Out of bound";
        System.out.println(message);
        return null;
    }
}