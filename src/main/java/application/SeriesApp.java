package application;

import repository.CategoryDAO;
import repository.SeriesDAO;
import services.CategoryService;
import services.SeriesService;


public class SeriesApp {


    public static void main(String[] args) {
        CategoryDAO categoryDAO = new CategoryDAO();
        CategoryService categoryService = new CategoryService(categoryDAO);

        SeriesDAO seriesDAO = new SeriesDAO();
        SeriesService seriesService = new SeriesService(seriesDAO);
//
//        PopularityDAO difficultyDAO = new PopularityDAO();
//        PopularityDAO difficultyService = new PopularityDAO(popularityDAO);


        Series series = new Series(categoryService, seriesService);

        series.startSeries();

    }
}
