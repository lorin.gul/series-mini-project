package application;

import domain.Category;
import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;
import services.CategoryService;
import services.SeriesService;

import java.sql.Connection;
import java.util.*;

public class Series {

    private Map<Integer, String> menuMap;
    private Scanner scanner;
    private CategoryService categoryService;
    private SeriesService seriesService;
    private Connection connection;

    /**
     * Aanmaken nieuw Serie object
     * uitvoering programma
     */

    public Series(CategoryService categoryService, SeriesService seriesService) {


        this.categoryService = categoryService;
        this.seriesService = seriesService;

        this.scanner = new Scanner(System.in);
        this.menuMap = new HashMap<>();

        menuMap.put(1, "Show the first serie category");
        menuMap.put(2, "Show serie number 1 in list");
        menuMap.put(3, "Show serie by name");
        menuMap.put(4, "Show series by year");
        menuMap.put(5, "Show all series");
    }



    /**
         * Start series app
         */




    public void startSeries() {
        System.out.println("**********Series DB**********");
        System.out.println("*********WELCOME Lorin*********");
        System.out.println("-Series options-");
        showMenu();
    }

    /**
     * Weergave menu map
     */
    private void showMenu() {
        for (Map.Entry<Integer, String> m : menuMap.entrySet()) {
            System.out.println(m.getKey() + " " + m.getValue());
        }
        chooseOption();
    }

    /**
     * Weergave van optie met nummer
     */
    private void chooseOption() {
        ;
        if (scanner == null) {
            scanner = new Scanner(System.in);
        }
        try {
            System.out.println("Choose your option : ");
            int i = scanner.nextInt();

            scanner.nextLine();
            executeOption(i);
        } catch (InputMismatchException e) {
            System.err.println("Not valid, please enter a number");
            scanner = null;
            chooseOption();
        }
    }
    /**
     * Bovengegeven ingegeven nummer van optie wordt uitgevoert
     * @param option - uitgevoerde nummer
     */
        private void executeOption ( int option){
            switch (option) {
                case 1:
                    findCategory(option);
                    break;

                case 2:
                    findSeries(1);
                    break;

                case 3:
                    insertAndFindSeriesByName();
                    break;

                case 4:
                   showSeriesByYear(1);
                    break;

                case 5:
                    showAllSeries();
                break;
            }
            boolean continueSeries = true;
            if (continueSeries) {
                showMenu();

            }
        }

    /**
     * Weergave id categorie
     * @param option categorie id
     */
        private void findCategory ( int option){
            try {
                Category category = categoryService.findCategoryById(option);
                System.out.println(category);
            } catch (NoRecordFoundException e) {
                e.getMessage();
                startSeries();
            }
        }
    /**
     * Weergave naam serie, director en network met opgegeven id
     * @param seriesId id van serie
     */
    private void findSeries ( int seriesId){
        try {
            System.out.println("Showing series with id" + seriesId);
            domain.Series series = SeriesService.findSeriesByID(seriesId);
            System.out.println("********GAME INFORMATION********");
            System.out.println("NAME:\t \t" + series.getSeriesName());
            System.out.println("DIRECTOR:\t" + series.getDirector());
            System.out.println("NETWORK:\t\t" + series.getNetwork());
            System.out.println("********************************");

        } catch (IDOutOfBoundException e) {
            e.printStackTrace();

        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        }

    }

    /**
     * Weergave series naam, director en jaar
     */
    private void showAllSeries(){
        try{
            System.out.println("Loading... Please wait...");
            List<domain.Series> serie = seriesService.findAllSeries();
            System.out.println("----- ALL SERIES -----");
            for(domain.Series series : serie){
                System.out.println("-----------------------------");
                System.out.println("ID:\t\t\t\t" + series.getId());
                System.out.println("NAME:\t\t" + series.getSeriesName());
                System.out.println("DIRECTOR:\t" + series.getDirector());
                System.out.println("YEAR:\t\t" + series.getYear());
                System.out.println("-----------------------------");
            }
        } catch (NoRecordFoundException e) {
            System.out.println(" /!\\ No result");
        }
    }
    /**
     * weergave jaar
     *
     * @param year ingave jaar, groter dan 0
     */
    private void showSeriesByYear(int year) {
        try{
            List<domain.Series> serie = seriesService.showSeriesFromYear(year);
            System.out.println("----- Series " + year + " -----");
            for(domain.Series series : serie){
                System.out.println("-----------------------------");
                System.out.println("ID:\t\t" + series.getId());
                System.out.println("NAME:\t\t" + series.getSeriesName());
                System.out.println("DIRECTOR:\t" + series.getDirector());
                System.out.println("YEAR:\t\t" + series.getYear());
                System.out.println("-----------------------------");
            }

            int newYear = askForYear();
            if(newYear > 0){
                showSeriesByYear(newYear);
            }

        } catch (NoRecordFoundException e) {
            System.out.println(" /!\\No result");
        }
    }
    /**
     * Vraag jaar
     *
     * @return opgegeven nummer
     */
    private int askForYear() {
        System.out.print("\nLoad year in format YYYY:");
        if(scanner.hasNextInt()){
            return scanner.nextInt();
        } else {
            askForYear();
        }
        return 0;
    }
    /**
     *Vraag naar serie naam
     * weergave resultaat
     */
    private void insertAndFindSeriesByName() {
        System.out.print("\nSearch: ");
        String match = scanner.nextLine();
        System.out.println("Searching......");
        findSeriesStartingWith(match);
    }
    /**
     * Opzoeking en weergave serie start met opgegeven String
     * @param match Begin naam voor het opzoeken van de serie
     */
    private void findSeriesStartingWith(String match){
        try {
            domain.Series series = seriesService.findSeriesStartingWith(match);
            System.out.println("----- GAME INFORMATION -----");
            System.out.println("NAME:\t\t" + series.getSeriesName());
            System.out.println("DIRECTOR:\t" + series.getDirector());
            System.out.println("YEAR:\t\t" + series.getYear());
            System.out.println("-----------------------------");
        } catch (NoRecordFoundException e) {
            System.out.println(" /!\\No matching series");
        }
    }

}
