package repository;

import domain.Category;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.testng.annotations.Test;
import utilities.DataBaseUtil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class CategoryDAOTest {
    private Connection connection;

    @BeforeEach
    public void connection() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/seriesTest", DataBaseUtil.USERNAME, "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void testFindCategoryByID(){
        CategoryDAO dao = new CategoryDAO(connection);
        Category result = dao.findCategoryById(1);
        Assertions.assertEquals("combination", result.getCategoryName());
    }

}
