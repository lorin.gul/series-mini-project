package repository;

import domain.Series;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.testng.annotations.Test;
import utilities.DataBaseUtil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public class SeriesDAOTest {
    private Connection connection;

    @BeforeEach
    public void connection(){
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/seriesTest", DataBaseUtil.USERNAME, "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void testFindAllSeries(){
        SeriesDAO dao = new SeriesDAO(connection);
        List<Series> results = dao.findAllSeries();

        Assertions.assertFalse(results.isEmpty());
    }
    @Test
    public void testShowGamesFromPage(){
        SeriesDAO dao = new SeriesDAO(connection);
        List<Series> results = dao.showSeriesFromYear(1);

        Assertions.assertFalse(results.isEmpty());
        Assertions.assertEquals(1, results.size());
    }


}
