package services;

import application.Series;
import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import repository.SeriesDAO;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)

public class SeriesServiceTest {

    @Mock
    private SeriesDAO dao;

    @InjectMocks
    SeriesService service;


    @Test
    @Tag("search")
    public void testFindSeriesByID(domain.Series series) throws IDOutOfBoundException, NoRecordFoundException {
        when(dao.findSeriesByID(1)).thenReturn(series);
        domain.Series result = SeriesService.findSeriesByID(1);
        verify(dao, times(1)).findSeriesByID(Mockito.anyInt());
        assertEquals(series, result);
    }
//    @Test @Tag("exceptions") @DisplayName("findAllSeries returns NoRecordFoundException when no series returned")
//    public void testFindALlSeriesThrowsNoRecordsFound(){
//        Assertions.assertThrows(NoRecordFoundException.class, () -> {
//            List<Series> series = new ArrayList<>();
//            when(dao.findAllSeries()).thenReturn(series);
//            service.findAllSeries();
//        });
//    }
}
