package services;

import domain.Popularity;
import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.testng.annotations.Test;
import repository.PopularityDAO;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)

public class PopularityServiceTest {

    @Mock
    PopularityDAO dao;

    @InjectMocks
    PopularityService service;

    @Test
    public void testFindPopularity() throws IDOutOfBoundException, NoRecordFoundException {
        Popularity popularity = new Popularity(1, "very popular");
        when(dao.findPopularityById(3))
                .thenReturn(popularity);

        Popularity result = service.findPopularityById(3);
        verify(dao, times(1)).findPopularityById(anyInt());
        Assertions.assertEquals(popularity, result);
    }
    @Test
    public void testDifficultyThrowsNoRecordExceptionWhenNoRecordsExist(){
        Assertions.assertThrows(NoRecordFoundException.class, () -> {
            when(dao.findPopularityById(5)).thenReturn(null);
            Popularity result = service.findPopularityById(5);
        });
    }

}
