package services;


import domain.Category;
import exceptions.NoRecordFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.testng.annotations.Test;
import repository.CategoryDAO;



import static org.mockito.Mockito.when;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)

public class CategoryServiceTest {

    @Mock
    private CategoryDAO dao;

    @InjectMocks
    CategoryService service;
    @Test
    public void testFindCategoryById() throws NoRecordFoundException {
        Category category = new Category();
        when(dao.findCategoryById(1)).thenReturn(category);
        Category category1 = service.findCategoryById(1);
        Assertions.assertEquals(category, category1);
        verify(dao, times(1)).findCategoryById(1);
    }

}
