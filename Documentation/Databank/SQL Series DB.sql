create database series;

use series;

CREATE TABLE  category(
	id           	integer       primary key auto_increment,
	category_name	varchar(30)	  not null
);

CREATE TABLE popularity(
   id             integer     primary key auto_increment,
   popularity_name		varchar(30)	not null
);


INSERT INTO  category VALUES (1,'romance');
INSERT INTO  category VALUES (2,'fantasy');
INSERT INTO  category VALUES (3,'drama');
INSERT INTO  category VALUES (4,'supernatural');
INSERT INTO  category VALUES (5,'mystery');
INSERT INTO  category VALUES (6,'history');
INSERT INTO  category VALUES (7,'music');
INSERT INTO  category VALUES (8,'psychological');
INSERT INTO  category VALUES (9,'comedy');
INSERT INTO  category VALUES (10,'action');


INSERT INTO popularity VALUES (1,'very popular');
INSERT INTO popularity VALUES (2,'popular');
INSERT INTO popularity VALUES (3,'average');
INSERT INTO popularity VALUES (4,'unpopular');
INSERT INTO popularity VALUES (5,'very unpopular');


CREATE TABLE series
(
   id               integer       primary key auto_increment,
   series_name      varchar(50)   not null,
   director        	varchar(50)   not null,
   year_edition		integer	not null,
   category_id      integer,
   episodes			integer,
   network			 varchar(50)   not null,
   popularity_id	integer,
foreign key (category_id) references  category(id),
foreign key (popularity_id) references popularity(id)
);



INSERT INTO series VALUES (1,'Boys Over Flowers','Jeon Ki Sang',2009,3,25,'KBS2',3);
INSERT INTO series VALUES (2,'Goblin','Lee Eung Bok',2017,3,16,'tvN',1);
INSERT INTO series VALUES (3,'You Who Came From The Stars','Oh Choong Hwan',2014,3,21,'SBS',1);
INSERT INTO series VALUES (4,'The Heirs','Kang Shin Hyo',2013,3,20,'SBS',1);
INSERT INTO series VALUES (5,'Reply 1994','Shin Won Ho',2013,3,21,'tvN',2);
INSERT INTO series VALUES (6,'Something In The Rain','Ahn Pan Seok',2018,1,16,'Netflix',2);
INSERT INTO series VALUES (7,'Man To Man','Lee Chang Min',2009,9,16,'jTBC',2);
INSERT INTO series VALUES (8,'Mianhae Saranghae','Lee Hyung Min',2004,1,16,'KBS2',1);
INSERT INTO series VALUES (9,'Love Alarm','Lee Na Jung',2019,9,8,'tvN',3);
INSERT INTO series VALUES (10,'7th Grade Civil Servant','Oh Hyun Jong',2013,10,20,'MBC',2);
INSERT INTO series VALUES (11,'Star Of The Universe','Kim Ji Hyun',2017,7,21,'Naver TV Cast', 3);
INSERT INTO series VALUES (12,'Kingdom','Kim Sung Hoon',2019,10,6,'Netflix',1);
INSERT INTO series VALUES (13,'Arthdal Chronicles Part 1','Kim Won Suk',2029,2,6,'tvN',1);
INSERT INTO series VALUES (14,'Long Time No See','Kang Woo',2017,10,5,'Naver TV Cast',3);
INSERT INTO series VALUES (15,'Signal','Kim Won Suk',2016,5,16,'tvN',1);
INSERT INTO series VALUES (16,'Healer','Kim Jin Woo',2015,10,20,'KBS2',2);
INSERT INTO series VALUES (17,'Mother','Yoon Hyun Gi',2018,8,16,'tvN',2);
INSERT INTO series VALUES (18,'SKY Castle','Jo Hyun Taek',2019,5,20,'jTBC',2);
INSERT INTO series VALUES (19,'Descendants Of The Sun',' Lee Eung Bok',2016,10,16,'KBS2',1);
INSERT INTO series VALUES (20,'Kill Me, Heal Me','Kim Dae Jin',2015,5,20,'MBC',1);




CREATE TABLE airing
(
    id              integer		primary key auto_increment,
    series_id       integer		not null,
    airing_start    datetime    not null,
    airing_end		datetime,
    foreign key (series_id) references series(id)
    );


INSERT INTO airing VALUES (1,1,'2009-01-05','2009-03-31');
INSERT INTO airing VALUES (2,2,'2016-12-02','2017-01-21');
INSERT INTO airing VALUES (3,3,'2013-12-18','2014-02-27');
INSERT INTO airing VALUES (4,4,'2013-10-09','2013-12-12');
INSERT INTO airing VALUES (5,5,'2013-10-18','2013-12-28');
INSERT INTO airing VALUES (6,6,'2018-03-30','2018-05-19');
INSERT INTO airing VALUES (7,7,'2017-04-21','2017-06-10');
INSERT INTO airing VALUES (8,8,'2004-10-09','2004-12-28');
INSERT INTO airing VALUES (9,9,'2019-08-22',null);
INSERT INTO airing VALUES (10,10,'2013-01-23','2013-03-28');
INSERT INTO airing VALUES (11,11,'2017-01-27','2017-02-10');
INSERT INTO airing VALUES (12,12,'2019-01-25',null);
INSERT INTO airing VALUES (13,13,'2019-06-01','2019-06-16');
INSERT INTO airing VALUES (14,14,'2017-10-23',null);
INSERT INTO airing VALUES (15,15,'2016-01-22','2016-03-12');
INSERT INTO airing VALUES (16,16,'2014-12-08','2015-02-10');
INSERT INTO airing VALUES (17,17,'2018-01-24','2018-03-15');
INSERT INTO airing VALUES (18,18,'2018-10-23','2019-02-01');
INSERT INTO airing VALUES (19,19,'2016-02-24','2016-04-14');
INSERT INTO airing VALUES (20,20,'2015-01-07','2015-03-12');